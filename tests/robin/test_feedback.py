"""
these tests should cover functions and classes in robin.py
"""
import pytest
from mock import AsyncMock

from bot.modules.robin import Robin

TESTSTRING = "Dit is een teststring"
SUCCESSFUL_FEEDBACK = "feedback sent successful"

ctx = AsyncMock(autospec=True)
robin = Robin()


@pytest.mark.asyncio
async def test_feedback():
    """
    These tests will check feedback result when everything is fine.
    """
    res = await robin._feedback(ctx=ctx, msg=TESTSTRING)
    assert res == SUCCESSFUL_FEEDBACK


@pytest.mark.asyncio
async def test_feedback_no_ctx():
    """
    These tests will check feedback result when contexts is nog spedified
    """
    res = await robin._feedback(msg=TESTSTRING)
    assert res == "context not spedified"


@pytest.mark.asyncio
async def test_feedback_delete_failed():
    """
    These tests will check feedback result when deletion of msg failed.
    """
    robin = Robin()
    context = AsyncMock()
    context.message.delete.side_effect = Exception("Boom!")
    res = await robin._feedback(ctx=context, msg=TESTSTRING, delete_message=True)
    assert res == "message deletion failed Boom!"


@pytest.mark.asyncio
async def test_feedback_delete_message_wrong_argument():
    """
    These tests will check feedback result when wrong argument is specified.
    """
    robin = Robin()
    context = AsyncMock()
    res = await robin._feedback(ctx=context, msg=TESTSTRING, delete_message=7)
    assert res == "Invallid option for delete_message 7"


@pytest.mark.asyncio
async def test_feedback_delete_message_fail():
    """
    These tests will check feedback result when deletion of msg succeeds.
    """
    robin = Robin()
    context = AsyncMock()
    res = await robin._feedback(ctx=context, msg=TESTSTRING, delete_message=True)
    assert res == SUCCESSFUL_FEEDBACK


@pytest.mark.asyncio
async def test_feedback_delete():
    """
    These tests will check feedback result when deletion of msg succeeds.
    """
    robin = Robin()
    context = AsyncMock()
    res = await robin._feedback(ctx=context, msg=TESTSTRING, delete_after=4)
    assert res == SUCCESSFUL_FEEDBACK
