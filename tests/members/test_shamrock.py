"""
testing tassadar
"""
import pytest
from mock import AsyncMock

from bot.modules.members import Members


@pytest.mark.asyncio
async def test_shamrock():
    """
    test_ping, expecting pong
    """
    members = Members()
    ctx = AsyncMock()
    members._feedback = AsyncMock()
    await members.shamrock(ctx)
    members._feedback.assert_called_once_with(
        ctx=ctx,
        msg="Voelt zich niet zo lekker,\n had gister dat laatste biertje niet moeten nemen...",
    )
