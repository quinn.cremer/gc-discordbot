"""
testing tassadar
"""
import pytest
from mock import AsyncMock

from bot.modules.members import Members


@pytest.mark.asyncio
async def test_s3v3n51n5():
    """
    test_ping, expecting pong
    """
    members = Members()
    ctx = AsyncMock()
    members._feedback = AsyncMock()
    await members.s3v3n51n5(ctx)
    members._feedback.assert_called_once_with(
        ctx=ctx,
        msg="Bûter, brea en griene tsiis, wa't dat net sizze kin is gjin oprjochte Fries",
    )
