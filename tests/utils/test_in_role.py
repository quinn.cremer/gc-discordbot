"""
these tests should cover functions and classes in roles.py
"""
from mock import MagicMock

from bot.modules.utils import in_role


def test_in_role_ok():
    """
    Testing to see if the user is in the specified role.
    """
    req_role = "testrole"
    role = MagicMock()
    role.name = req_role
    ctx = MagicMock()
    ctx.author.roles = [role]
    res = in_role(ctx, req_role)
    assert res is True


def test_in_role_nok():
    """
    Testing to see if the user is not in the specified role.
    """
    req_role = "other_role"
    role = MagicMock()
    role.name = "testrole"
    ctx = MagicMock()
    ctx.author.roles = [role]
    res = in_role(ctx, req_role)
    assert res is False
