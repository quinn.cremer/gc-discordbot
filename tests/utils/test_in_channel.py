"""
these tests should cover functions and classes in roles.py
"""
import pytest
from mock import AsyncMock, MagicMock
from mock.mock import patch

from bot.modules.utils import get_channel_id_by_name


@pytest.mark.asyncio
async def test_get_channel_id_by_name_ok():
    """
    Testing to get a valid channel id.
    """
    req_channel = "testchannel"
    channel = MagicMock()
    channel.name = req_channel
    channel.id = 7
    ctx = MagicMock()
    ctx.guild.channels = [channel]
    res = await get_channel_id_by_name(ctx, req_channel)
    assert res == 7


@pytest.mark.asyncio
async def test_get_channel_id_by_name_nok():
    """
    Testing to get a valid channel id.
    """
    req_channel = "testchannel_notfound"
    channel = MagicMock()
    channel.name = "testchannel"
    channel.id = 7
    ctx = MagicMock()
    ctx.guild.channels = [channel]
    res = await get_channel_id_by_name(ctx, req_channel)
    assert res == 0
