#!/usr/bin/env python
import os
import re
import sys

import semver


def bump_fix(latest: str) -> str:
    """Increase the version for a fix/patch

    Args:
        latest (str): the latest version (semver)

    Returns:
        str: the patch increased version
    """
    return semver.bump_patch(latest)


def bump_feature(latest: str) -> str:
    """Increase the version for a feature/minor

    Args:
        latest (str): the latest version (semver)

    Returns:
        str: the minor increased version
    """
    return semver.bump_minor(latest)


def bump_release(latest: str) -> str:
    """Increase the version for a release/major

    Args:
        latest (str): the latest version (semver)

    Returns:
        str: the major increased version
    """
    return semver.bump_major(latest)


def invalid_action(latest: str):
    """invalid action was passed.

    Args:
        latest (str): the latest version (semver)

    Raises:
        Exception: invalid branchname
    """
    raise Exception("Invalid branchname")


def get_what_to_bump(latest: str) -> str:
    """Figure out what to increase..

    Args:
        latest (str): the latest version (semver)

    Returns:
        str: the next version (based on merged branchname)
    """

    actions = {
        "fix": bump_fix,
        "patch": bump_fix,
        "feature": bump_feature,
        "minor": bump_feature,
        "release": bump_release,
        "major": bump_release,
    }

    commit_tittle = os.getenv("CI_COMMIT_TITLE", default="feature/missing")
    action = re.findall(r"Merge branch '(.+)/.+' into 'main'", commit_tittle)[0]
    bump_function = actions.get(action, invalid_action)

    return bump_function(latest)


def main(current):
    version = get_what_to_bump(current)
    print(version)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1]))
