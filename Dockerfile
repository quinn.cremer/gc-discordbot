#FROM --platform=$BUILDPLATFORM python:3-alpine3.15
#
#RUN apk --no-cache upgrade  && \
#pip install pipenv
## RUN mkdir bot
#WORKDIR /bot
#
#
#COPY Pipfile /bot
#COPY bot/run.py bot/Pipfile bot/__init__.py /bot/
#COPY bot/modules /bot/modules
#RUN cd /bot && pipenv install
#
## ENTRYPOINT python bot.py



FROM alpine:latest AS base
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache py3-pip py3-setuptools py3-virtualenv tiff-dev jpeg-dev \
    openjpeg-dev zlib-dev freetype-dev lcms2-dev  libwebp-dev tcl-dev tk-dev harfbuzz-dev \
    fribidi-dev libimagequant-dev libxcb-dev libpng-dev
WORKDIR /bot


FROM base AS builder
RUN apk add --no-cache musl-dev gcc g++ python3-dev libffi-dev openssl-dev cargo
ADD bot/requirements.txt /bot/.
RUN python3 -m venv venv && \
        pip install wheel && \
        . venv/bin/activate && \
	pip install -U pip && \
        pip install -r requirements.txt


FROM base
COPY .container_version /.container_version
COPY bot/ /bot/
COPY --from=builder /bot/venv /bot/venv
CMD . venv/bin/activate && ./run.py
