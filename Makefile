ENVIRONMENT=gc-tst
CONTAINER_NAME=discordbot
.DEFAULT_GOAL=help
SHELL=/bin/sh

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: full_test
full_test: init ## deploy vpc stack
	pytest --cov-report xml:tests/test-results/coverage.xml --cov
	pylint bot/ tests/ -r n — msg-template='/path}:{line}: [{msg_id}({symbol}), {obj}] {msg}' | tee tests/test-results/pylint.txt
	docker run \
    --rm \
	-e SONAR_HOST_URL="https://sonarcloud.io" \
    -e SONAR_LOGIN=${SONAR_TOKEN} \
    -v "${PWD}:/usr/src" \
    sonarsource/sonar-scanner-cli -Dsonar.branch.name=${GITHUB_HEAD_REF}

.PHONY: test
test: init ## running tests local
	pytest --cov-report xml:tests/test-results/coverage.xml --cov
	# pipenv run pylint bot/ tests/ -r n — msg-template='/path}:{line}: [{msg_id}({symbol}), {obj}] {msg}' | tee tests/test-results/pylint.txt

.PHONY: init
init: ## initializes the python environment
	pip install --ignore-installed -r requirements-dev.txt


.PHONY: build
build: init ## build container
	docker login -u rvegmond -p $(DOCKER_ACCESS_TOKEN)
	$(eval CURRENT_VERSION := $(shell curl --silent https://hub.docker.com/v2/repositories/rvegmond/$(CONTAINER_NAME)/tags |jq ".results[].name" |tr -d \" |grep -v latest |sort  --version-sort|tail -1))
	$(eval NEXT_VERSION := $(shell bin/gen-semver.py $(CURRENT_VERSION)))
	@@echo "CONTAINER_NAME__$(CONTAINER_NAME)__"
	@@echo "CURRENT_VERSION__$(CURRENT_VERSION)__"
	@@echo "NEXT_VERSION__$(NEXT_VERSION)__"
	@@echo "container_version = $(NEXT_VERSION)" >> .container_version
	docker build -t rvegmond/$(CONTAINER_NAME):$(NEXT_VERSION) .
	docker push rvegmond/$(CONTAINER_NAME):$(NEXT_VERSION)

.PHONY: build-local
build-local: init ## build container local, without loging in.
	@@echo "CONTAINER_NAME__$(CONTAINER_NAME)__"
	$(eval NEXT_VERSION := 0.0.0)
	@@echo "CONTAINER_NAME__$(CONTAINER_NAME)__"
	@@echo "NEXT_VERSION__$(NEXT_VERSION)__"
	@@echo "$(NEXT_VERSION)" > .container_version
	docker build -t rvegmond/$(CONTAINER_NAME):$(NEXT_VERSION) .

.PHONY: build-test
build-test: init ## build container local, without loging in.
	@@echo "CONTAINER_NAME__$(CONTAINER_NAME)__"
	@@echo test > .container_version
	docker build -t rvegmond/$(CONTAINER_NAME):test .
