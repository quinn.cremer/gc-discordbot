from loguru import logger

# TODO: need to rewrite _get_channel_by_name is copied from old setup.


def _get_channel_by_name(self, channel_name: str):
    """
    Get the channel for a channel_name.
    paramters:
        channel_name:        The channel where to fetch channel for.
    """
    all_channels = self.static.get("all_guild_channels")
    for channel in all_channels:
        if channel.get("name") == channel_name:
            return channel.get("channel")
    logger.info("channel_not_found")
    return "channel_not_found"
