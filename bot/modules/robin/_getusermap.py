# TODO: need to rewrite _getusermap is copied from old setup.
def _getusermap(self, memberid):
    """
    Get the mapping for discordalias and gsheetalias
    Id is the key for the selection.
    with the provided alias.
    """
    usermap = {}
    usermap = (
        self.db.session.query(
            self.db.User.UserId,
            self.db.User.DiscordId,
            self.db.User.DiscordAlias,
            self.db.User.GsheetAlias,
        ).filter(self.db.User.UserId == memberid)
    ).one()
    return usermap
