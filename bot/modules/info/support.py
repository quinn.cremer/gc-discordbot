from discord.ext import commands


@commands.command(
    name="support",
    aliases=["support_modules"],
    help=("Geeft info over de support modules van een speler."),
    brief=("Geeft info over de support modules van een speler."),
)
async def support(self, ctx, *args):
    command = "support"
    if len(args) == 0:
        await self._parse_module_image(ctx=ctx, command=command)
    else:
        if args[0] in ["ws1", "ws2", "ws3"]:
            await self._parse_wsmodule_image(ctx=ctx, command=command, ws=args[0])
        else:
            await self._parse_module_image(ctx=ctx, command=command, userid=args[0])
