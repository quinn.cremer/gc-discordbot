MINING_IMAGES = [
    [
        {"MiningBoost": "miningboost.png"},
        {"HydrogenBayExtension": "hydrogenbayextension.png"},
        {"Enrich": "enrich.png"},
        {"RemoteMining": "remotemining.png"},
    ],
    [
        {"HydrogenUpload": "hydrogenupload.png"},
        {"MiningUnity": "miningunity.png"},
        {"Crunch": "crunch.png"},
        {"Genesis": "genesis.png"},
    ],
    [
        {"HydrogenRocket": "hydrogenrocket.png"},
        {"MiningDrone": "miningdrone.png"},
        {"none": "blank.png"},
        {"none": "blank.png"},
    ],
]


SHIELD_IMAGES = [
    [
        {"AlphaShield": "alphashield.png"},
        {"DeltaShield": "deltashield.png"},
        {"PassiveShield": "passiveshield.png"},
        {"OmegaShield": "omegashield.png"},
    ],
    [
        {"MirrorShield": "mirrorshield.png"},
        {"BlastShield": "blastshield.png"},
        {"AreaShield": "areashield.png"},
        {"none": "blank.png"},
    ],
]


SUPPORT_IMAGES = [
    [
        {"EMP": "emp.png"},
        {"Teleport": "teleport.png"},
        {"RedStarLifeExtender": "redstarlifeextender.png"},
        {"RemoteRepair": "remoterepair.png"},
    ],
    [
        {"TimeWarp": "timewarp.png"},
        {"Unity": "unity.png"},
        {"Sanctuary": "sanctuary.png"},
        {"Stealth": "stealth.png"},
    ],
    [
        {"Fortify": "fortify.png"},
        {"Impulse": "impulse.png"},
        {"AlphaRocket": "alpharocket.png"},
        {"Salvage": "salvage.png"},
    ],
    [
        {"Suppress": "suppress.png"},
        {"Destiny": "destiny.png"},
        {"Barrier": "barrier.png"},
        {"Vengeance": "vengeance.png"},
    ],
    [
        {"DeltaRocket": "deltarocket.png"},
        {"Leap": "leap.png"},
        {"Bond": "bond.png"},
        {"LaserTurret": "laser_turret.png"},
    ],
    [
        {"AlphaDrone": "alphadrone.png"},
        {"Suspend": "mod_timeslow_icon.png"},
        {"OmegaRocket": "omegarocket.png"},
        {"RemoteBomb": "remotebomb.png"},
    ],
]

TRADE_IMAGES = [
    [
        {"CargoBayExtension": "cargobayextension.png"},
        {"ShipmentComputer": "shipmentcomputer.png"},
        {"TradeBoost": "tradeboost.png"},
        {"Rush": "rush.png"},
    ],
    [
        {"TradeBurst": "tradeburst.png"},
        {"ShipmentDrone": "shipmentdrone.png"},
        {"Offload": "offload.png"},
        {"ShipmentBeam": "shipmentbeam.png"},
    ],
    [
        {"Entrust": "entrust.png"},
        {"Dispatch": "dispatch.png"},
        {"Recall": "recall.png"},
        {"RelicDrone": "relic_drone.png"},
    ],
]

WEAPON_IMAGES = [
    [
        {"WeakBattery": "weakbattery.png"},
        {"Battery": "battery.png"},
        {"Laser": "laser.png"},
        {"MassBattery": "massbattery.png"},
    ],
    [
        {"DualLaser": "duallaser.png"},
        {"Barrage": "barrage.png"},
        {"DartLauncher": "dartlauncher.png"},
        {"none": "blank.png"},
    ],
]
MODULE_IMAGES = {}
MODULE_IMAGES["mining"] = MINING_IMAGES
MODULE_IMAGES["shield"] = SHIELD_IMAGES
MODULE_IMAGES["support"] = SUPPORT_IMAGES
MODULE_IMAGES["trade"] = TRADE_IMAGES
MODULE_IMAGES["weapon"] = WEAPON_IMAGES

images_col1 = []
images_col2 = []
images = []
images_col1.extend(TRADE_IMAGES)
images_col1.extend(MINING_IMAGES)
images_col1.extend(WEAPON_IMAGES)
images_col2.extend(SHIELD_IMAGES)
images_col2.extend(SUPPORT_IMAGES)
image_length = len(images_col1)
for i in range(image_length):
    images.append(images_col1[i] + images_col2[i])
MODULE_IMAGES["info"] = images
