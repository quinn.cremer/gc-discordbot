import os

import discord
from icecream import ic
from PIL import Image, ImageDraw, ImageFont
from sqlalchemy import inspect

font = "Roboto-Bold.ttf"
font_path = "fonts/Roboto-Bold.ttf"
image_size = 84
font_size = 30
color = {}
color["support"] = (198, 253, 169)
color["trade"] = (253, 242, 169)
color["weapon"] = (241, 185, 199)
color["mining"] = (195, 179, 250)
color["shield"] = (181, 253, 230)
color["info"] = (182, 253, 230)


def add_text(
    image, row, column, command, text="12", font=font_path, font_size=font_size
):
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype(font, size=font_size)
    p1 = (
        (column + 1) * 84 + column * font_size * 2,
        row * font_size + (row + 1) * 84 - font_size,
    )
    draw.text(p1, text, fill=color[command], font=font)
    return image


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}


def get_module_level(db, userid, module):
    result = (
        db.session.query(db.Gsheet)
        .join(db.User, db.Gsheet.Naam == db.User.GsheetAlias)
        .filter(db.User.UserId == userid)
    )
    if result.count() == 1:
        return object_as_dict(result.first())[module]


def _generate_module_image(self, userid, module_list, command):
    y_size = len(module_list)
    if y_size < 6:
        y_size = 6
    x_size = len(module_list[0])
    ic(x_size)
    ic(x_size * 84 + x_size * font_size * 2)
    ic(y_size)
    ic((84 + font_size) * y_size)

    image_size = (x_size * 84 + x_size * font_size * 2, (84 + font_size) * y_size)
    new_image = Image.new("RGBA", image_size)
    for row, column in enumerate(module_list):
        for index, image_dict in enumerate(column):
            image = "/".join(["images", list(image_dict.values())[0]])
            tmp_image = Image.open(image).convert("RGBA")
            module = list(image_dict.keys())[0]
            if module != "none":
                module_level = get_module_level(
                    db=self.db, userid=userid, module=module
                )
            else:
                module_level = ""
            new_image.paste(
                tmp_image, (index * (84 + font_size * 2), row * (84 + font_size))
            )
            new_image = add_text(
                image=new_image,
                text=str(module_level),
                row=row,
                command=command,
                column=index,
                font_size=font_size,
            )
    return new_image


async def _parse_module_image(self, ctx, command, userid=None, heading=None):
    images = []
    if userid is None:
        userid = ctx.author.id
    # elif 'ws' in userid:
    # do ws stuff
    else:
        userid = self._get_userid(db=self.db, user=str(userid))
    if self._has_gsalias(userid):
        # _generate_image_matrix(type='user', user=userid, command=command, db=self.db)
        image_name = "".join([str(userid), ".png"])
        images = self.MODULE_IMAGES[command]
        image = self._generate_module_image(
            userid=userid, module_list=images, command=command
        )
        image.save(image_name, "PNG")
        if heading is None:
            e = discord.Embed(
                title=(
                    f"{command.title()} modules for {self._get_username_by_id(userid)}"
                )
            )
        else:
            e = discord.Embed(
                description=heading, title=self._get_username_by_id(userid)
            )
        e.set_image(url=f"attachment://{image_name}")
        await ctx.channel.send(file=discord.File(image_name), embed=e)
        os.remove(image_name)
    else:
        msg = (
            f"Geen google alias gevonden voor {userid}, "
            " je kunt deze zetten met `!gsalias <google aliasnaam>`"
        )
        await self._feedback(ctx=ctx, msg=msg)
