from discord.ext import commands


@commands.command(
    name="weapon",
    aliases=["weapons"],
    help=("Geeft info over de weapon modules van een speler."),
    brief=("Geeft info over de weapon modules van een speler."),
)
async def weapon(self, ctx, *args):
    command = "weapon"
    if len(args) == 0:
        await self._parse_module_image(ctx=ctx, command=command)
    else:
        if args[0] in ["ws1", "ws2", "ws3"]:
            await self._parse_wsmodule_image(ctx=ctx, command=command, ws=args[0])
        else:
            await self._parse_module_image(ctx=ctx, command=command, userid=args[0])
