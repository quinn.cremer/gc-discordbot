from datetime import datetime

from discord.ext import commands, tasks
from loguru import logger

from ..utils import feedback, get_channel_by_name, normalize_time, rolemembers

########################################################################################
#   _update_comeback_channel
########################################################################################


async def _update_comeback_channel(db, bot, comeback_channel, which_ws):
    logger.info("in _update_comeback_channel")

    get_comback = (
        db.session.query(
            db.User.DiscordAlias,
            db.WSComeback.UserId,
            db.WSComeback.WSId,
            db.WSComeback.ShipType,
            db.WSComeback.ReturnTime,
            db.WSComeback.NotificationTime,
        )
        .filter(db.WSComeback.NotificationTime > datetime.now())
        .filter(db.WSComeback.WSId == which_ws)
        .join(db.User)
    )

    await comeback_channel.purge(limit=100)
    msg = (
        "In dit kanaal staat het overzicht wanneer schepen weer terug de ws in mogen, "
        f"dit kanaal is specifiek voor {which_ws}.\n"
        f"Met: **`{bot.command_prefix}terug <schip> <terugkomtijd>`** geef je aan "
        "wanneer jouw schip weer terug de ws in mag. Het is ook mogelijk een "
        " notificatietijd toe te voegen, dit doe je met "
        f"**`{bot.command_prefix}terug <schip> <terugkomtijd> <notificatietijd>`**\n\n"
        "Voor schip kan gebruikt worden  **bs**, **ukkie** of **drone**.\n"
        "Als tijdformaat kun je drie formaten gebruiken:\n"
        "   **uu:mm**   - dit zijn de uren en minuten vanaf nu, "
        "dit is bijvoorbeeld voor een ukkie of een bs\n"
        "   **uu:mm*u***  - dit is het tijdstip op de klok in een 24 uurs notatie "
        "(18:00), dit is bijvoorbeeld voor"
        " de notificatie, Robin houdt rekening met de dagwissel.\n"
        "   **uu.t**    - dit is de tijd waarin t tienden van een uur zijn, "
        " dit is bijvoorbeeld voor drones.\n"
        "Bijvoorbeeld:\n"
        f"**`{bot.command_prefix}terug bs 17:00 8:00u`** - ik kan over 17 uur vanaf "
        "nu weer een bs insturen maar wil morgen om 8:00 pas een notificatie.⁣\n"
        "\u2063"
    )

    await comeback_channel.send(msg)
    msg = "**Speler     Schip     TerugTijd     NotificatieTijd**\n"
    if get_comback.count() > 0:
        for item in get_comback.all():
            returntime = item.ReturnTime.strftime("%a %H:%M")
            notificationtime = item.NotificationTime.strftime("%a %H:%M")
            msg += (
                f"**{item.DiscordAlias}**      {item.ShipType}         "
                f"{returntime}       {notificationtime}\n"
            )
    await comeback_channel.send(msg)


########################################################################################
#  command terug
########################################################################################


@commands.command(
    name="terug",
    help=(
        "Als je een schip verloren bent krijg je hiermee een seintje als je er weer in "
        "mag, dit is ook erg handig voor je mede spelers en de planners.\n"
        "!terug <schip> terugkomtijd  notificatietijd\n"
        "schiptype: bs of ukkie of drone\n"
        "tijdnotatie (zowel terugkom als notificatietijd):\n"
        "- XX:YY   uren:minuten worden bij huidige tijd geteld "
        "(verlies van een schip)\n"
        "- XX:YYu is de tijd op de klok, is voor de notificatietijd, "
        " Robin corrigeert voor de dag\n"
        "- XX.Y    uren en 10en van uren, wordt omgerekend naar minuten "
        "(voor een drone)\n"
        "Zonder opgave van notificatietijd, krijg je ene notificatie op het moment "
        "van je terugkomtijd.\n"
    ),
    brief="Meld de terugkomtijd van je schip aan.",
)
async def terug(self, ctx, *args):
    comeback_channel = {}
    comeback_channel["WS1"] = get_channel_by_name(
        ctx, dict(self.bot.settings).get("WS1").get("COMEBACK_CHANNEL_NAME"),
    )
    comeback_channel["WS2"] = get_channel_by_name(
        ctx, dict(self.bot.settings).get("WS2").get("COMEBACK_CHANNEL_NAME"),
    )

    usermap = self._getusermap(int(ctx.author.id))
    returntime = normalize_time(args[1])
    logger.info(f"len(args) {len(args)}")
    logger.info(f"args {args}")
    if len(args) == 2:
        notificationtime = returntime
    elif len(args) == 3:
        notificationtime = normalize_time(args[2])
    else:
        # send help!
        await ctx.send_help(ctx.command)
        return

    shiptype = args[0].lower()
    if shiptype in ["bs", "ukkie", "uk", "ts", "miner", "drone"]:
        result = (
            self.db.session.query(self.db.WSComeback)
            .filter(
                self.db.WSComeback.UserId == usermap["UserId"],
                self.db.WSComeback.ShipType == shiptype,
            )
            .count()
        )
        logger.info(f"count: {result}")
        if result > 0:
            self.db.session.query(self.db.WSComeback).filter(
                self.db.WSComeback.UserId == usermap["UserId"],
                self.db.WSComeback.ShipType == shiptype,
            ).delete()
    else:
        # wrong shiptype, send help!
        await ctx.send_help(ctx.command)
        return

    ws = None
    for wslist in ["WS1", "WS2"]:
        if usermap["UserId"] in rolemembers(ctx=ctx, role_name=wslist):
            ws = wslist
    new_return = self.db.WSComeback(
        UserId=usermap["UserId"],
        WSId=ws,
        ShipType=shiptype,
        ReturnTime=datetime.strptime(returntime, "%Y-%m-%d %H:%M"),
        NotificationTime=datetime.strptime(notificationtime, "%Y-%m-%d %H:%M"),
    )
    self.db.session.add(new_return)
    self.db.session.commit()

    await _update_comeback_channel(
        bot=self.bot, db=self.db, comeback_channel=comeback_channel[ws], which_ws=ws,
    )
    if shiptype == "drone":
        await feedback(
            ctx,
            msg=(
                f"{usermap['DiscordAlias']}, succes met ophalen van "
                "relics, straks snel weer een nieuwe drone"
            ),
            delete_after=3,
            delete_message=True,
        )
    else:
        await feedback(
            ctx,
            msg=(
                f"Helaas, {usermap['DiscordAlias']}, hopelijk volgende "
                f"keer meer succes met je {shiptype}"
            ),
            delete_after=3,
            delete_message=True,
        )


def _get_channel_by_name(channel_name: str, bot):
    """
    Get the channel for a channel_name.
    paramters:
        channel_name:        The channel where to fetch channel for.
    """
    all_channels = bot.static.get("all_guild_channels")
    for channel in all_channels:
        if channel.get("name") == channel_name:
            return channel.get("channel")
    logger.info("channel_not_found")
    return "channel_not_found"


########################################################################################
#  runner return_scheduler
########################################################################################
@tasks.loop(minutes=1)
async def return_scheduler(self):
    """
    this is the "cron" for the comeback notifications
    """
    comeback_channel = {}
    ws_channel = {}
    comeback_channel["WS1"] = _get_channel_by_name(
        channel_name="ws1-comeback", bot=self.bot,
    )
    comeback_channel["WS2"] = _get_channel_by_name(
        channel_name="ws2-comeback", bot=self.bot,
    )
    ws_channel["WS1"] = _get_channel_by_name(
        channel_name="ws1-white-star", bot=self.bot,
    )
    ws_channel["WS2"] = _get_channel_by_name(
        channel_name="ws2-white-star", bot=self.bot,
    )
    to_notify = self.db.session.query(
        self.db.WSComeback.UserId,
        self.db.WSComeback.WSId,
        self.db.WSComeback.ShipType,
        self.db.WSComeback.ReturnTime,
        self.db.WSComeback.NotificationTime,
    ).filter(
        self.db.WSComeback.NotificationTime
        == datetime.strptime(
            datetime.now().strftime("%Y-%m-%d %H:%M"), "%Y-%m-%d %H:%M",
        ),
    )
    if to_notify.count() > 0:
        for item in to_notify.all():
            await ws_channel[item.WSId].send(
                f"<@{item.UserId}>, je {item.ShipType} mag de ws in, succes!",
            )
            await _update_comeback_channel(
                bot=self.bot,
                db=self.db,
                comeback_channel=comeback_channel[item.WSId],
                which_ws=item.WSId,
            )
