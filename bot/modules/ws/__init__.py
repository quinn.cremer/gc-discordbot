"""
All related to whitestar functionality
"""
import locale

from loguru import logger

from ..robin import Robin

try:
    locale.setlocale(locale.LC_ALL, "nl_NL.utf8")  # required running on linux
    logger.info("running on linux")
except locale.Error:
    locale.setlocale(locale.LC_ALL, "nl_NL.UTF-8")  # required when running on MAC
    logger.info("running on mac")


class WS(Robin):
    from .comeback import return_scheduler, terug
    from .entry import _ws_entry, updateusermap, ws
    from .idee import idee
    from .status import status

    def __init__(self, bot=None, db=None):
        super().__init__(bot=bot, db=db)
        self.return_scheduler.start()
        logger.info(f"Class {type(self).__name__} initialized ")
