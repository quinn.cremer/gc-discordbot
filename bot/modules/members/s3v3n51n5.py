"""
The contents of this file is to reflect happyness of robin.
"""
from discord.ext import commands


@commands.command(
    name="s3v3n51n5",
    help="Hoe is het met Seven?",
    brief="Hoe is het met Seven?",
)
async def s3v3n51n5(self, ctx):
    """
    How are you Seven?
    """
    await self._feedback(
        ctx=ctx,
        msg="Bûter, brea en griene tsiis, wa't dat net sizze kin is gjin oprjochte Fries",
    )
