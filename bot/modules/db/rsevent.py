#!/usr/bin/env python


from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Table

from . import Base


class RSEvent(Base):
    __tablename__ = "rsevent"
    EventId = Column(Integer, primary_key=True)
    DiscordId = Column(Integer, ForeignKey("user.UserId"))
    RSLevel = Column(Integer)
    Runtime = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    RunId = Column(Integer, ForeignKey("rsrun-list.RunId"))

    def __repr__(self):
        return (
            f"<RSEvent(EventId={self.EventId},"
            f"RSEvent(DiscordId={self.DiscordId},"
            f"RSEvent(RSLevel={self.RSLevel},"
            f"RsEvent(Runtime={self.Runtime}"
            f"RsEvent(RunId={self.RunId}>"
        )
