"""
The contents of this file is to reflect happyness of robin.
"""
from discord.ext import commands

# RSEVENT_STARTIME = "2024-02-12 17:00"

###############################################################################################
#  command status
###############################################################################################


@commands.command(
    name="rsevent",
    help=(
        "Met het status commando update je status in het status kanaal,"
        " hiermee help je je mede ws-ers op de hoogte te houden hoe snel je kunt reageren."
    ),
    brief="Update je status in het status kanaal",
)
async def rsevent(self, ctx, *args):
    """
    updating the status of ws participants
    """
    await self._update_rsevent_table()
