"""
this is the init file for the bot
"""
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__)))


class Main:
    """
    The master class for Robin.
    """

    from ._get_env_settings import _get_env_settings
    from ._run_bot import _run_bot
