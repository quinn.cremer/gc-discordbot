import json

from dotenv import dotenv_values

DEFAULT_SETTINGS = {
    "EVENTLIST_CHANNEL": "rs-eventlist",
    "STATUS_CHANNEL_NAME": "ws-status",
    "WS_ROLE_NAME": "ws inschrijven",
    "WSIN_CHANNEL_NAME": "ws-inschrijvingen",
    "WSLIST_CHANNEL_NAME": "ws-inschrijflijst",
    "OFFICERS_CHANNEL_NAME": '{"WS1": "ws1-officers", "WS2": "ws2-officers"}',
    "PLANNER_ROLE_NAME": '{"WS1": "Planning WS 1", "WS2": "Planning WS 2"}',
    "CHANNEL_NAME": '{"WS1": "ws1-white-star", "WS2": "ws2-white-star"}',
    "COMEBACK_CHANNEL_NAME": '{"WS1": "ws1-comeback", "WS2": "ws2-comeback"}',
}


all_settings = [
    "BOT_DESCRIPTION",
    "CHANNEL_NAME",
    "COMEBACK_CHANNEL_NAME",
    "COMMAND_PREFIX",
    "DISCORD_TOKEN",
    "ENV_NAME",
    "EVENTLIST_CHANNEL",
    "GUILD_ID",
    "OFFICERS_CHANNEL_NAME",
    "PLANNER_ROLE_NAME",
    "STATUS_CHANNEL_NAME",
    "WS_ROLE_NAME",
    "WSIN_CHANNEL_NAME",
    "WSLIST_CHANNEL_NAME",
]


def _get_env_settings(self):
    tmp_settings = {}
    settings = DEFAULT_SETTINGS
    dotenv_settings = dotenv_values()  # take environment variables from .env.
    settings.update(dotenv_settings)
    for setting in [
        "OFFICERS_CHANNEL_NAME",
        "PLANNER_ROLE_NAME",
        "CHANNEL_NAME",
        "COMEBACK_CHANNEL_NAME",
    ]:
        for ws in ["WS1", "WS2"]:
            try:
                value = json.loads(settings[setting])[ws]
                tmp_settings[ws][setting] = value
            except KeyError:
                tmp_settings[ws] = {}
                value = json.loads(settings[setting])[ws]
                tmp_settings[ws][setting] = value
        settings.pop(setting)
    settings.update(tmp_settings)
    return settings
